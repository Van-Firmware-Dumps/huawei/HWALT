// every line must contain '//' or setting
// whole line is commented if '//' is found
// this file is parsed by 'find' function
// make sure no space is left @ end of file
//
// should set modelFolder - modelName - nnHardWare
//
// modelFolder : folder contains ms file
modelFolder = /odm/etc/camera/cnnmodel/AITMC
//
// modelName : ms name
modelName = /aitmc_2i1o_1ch_8a8f_ljq230112.ms
//
// input tensor number of network
// supported : 1 / 2
nnInputNum = 2
//
// ipuIoType : ipu in / out type
// supported : u8u8 f16f16 u8f16 u8u16(experiment for 16a8w)
ipuIoType = u8f16
//
// nnHardWare : nnHardWare
// supported : HTP / GPU
nnHardWare = HTP
// nnAccuracy : accuracy for network, set while quanti
// supported : 8a8w / float16
// now only judge if is float16
nnAccuracy = 8a8w
// sigmoidOnC : do sigmoid on C
// recognised : true / True / TRUE
sigmoidOnC = True
// useFaceRect : use face rect as 2nd input
// recognised : true / True / TRUE
useFaceRect = False
//
// !! keep this comment line as last line !!